/********************************************************************************/
/***File Name:sx1278.c                                                        ***/
/***Function :通过模拟SPI对sx1278芯片配置，实现Master or Slave Mode           ***/
/********************************************************************************/

#include "sx1278.h"
#include "spi.h"

//lpCtrlTypefunc_t lpTypefunc = { 0, 0, 0 };
unsigned char power_data[8] = { 0X80, 0X80, 0X80, 0X83, 0X86, 0x89, 0x8c, 0x8f };

unsigned char Frequency[3] = { 0x75, 0x80, 0x00 };//470Mhz
//unsigned char Frequency[3] = { 0x6c, 0x80, 0x00 };//430Mhz ????
unsigned char powerValue = 7;
unsigned char SpreadingFactor = 7;    //????7-12
unsigned char CodingRate = 1;        //1-4
unsigned char Bw_Frequency = 7;      //??6-9
unsigned char RF_EX0_STATUS;
unsigned char CRC_Value;
unsigned char SX1278_RLEN;

void Delay1s(unsigned int ii)
{
	int j;
	while(ii--)
	{
		for(j=0;j<1000;j++);
	}
}

void  SX1276LoRaSetOpMode(RFMode_SET opMode)
{
	unsigned char opModePrev;
	opModePrev = SX1276Read(REG_LR_OPMODE);
	opModePrev &= 0xf8;
	opModePrev |= (unsigned char) opMode;
	SX1276Write( REG_LR_OPMODE, opModePrev);
}

void  SX1276LoRaSetRFFrequency(void)
{
	SX1276Write( REG_LR_FRFMSB, Frequency[0]);//0x04???????????
	SX1276Write( REG_LR_FRFMID, Frequency[1]);//0x07???????????
	SX1276Write( REG_LR_FRFLSB, Frequency[2]);//0x00???????????
}

void  SX1276LoRaSetNbTrigPeaks(unsigned char value)
{
	unsigned char RECVER_DAT;
	RECVER_DAT = SX1276Read(0x31);
	RECVER_DAT = (RECVER_DAT & 0xF8) | value;
	SX1276Write(0x31, RECVER_DAT);
}

void  SX1276LoRaSetSpreadingFactor(unsigned char factor)
{
	unsigned char RECVER_DAT;
	SX1276LoRaSetNbTrigPeaks(3);
	RECVER_DAT = SX1276Read( REG_LR_MODEMCONFIG2);
	RECVER_DAT = (RECVER_DAT & RFLR_MODEMCONFIG2_SF_MASK) | (factor << 4);
	SX1276Write( REG_LR_MODEMCONFIG2, RECVER_DAT);
}

void  SX1276LoRaSetErrorCoding(unsigned char value)
{
	unsigned char RECVER_DAT;
	RECVER_DAT = SX1276Read( REG_LR_MODEMCONFIG1);
	RECVER_DAT = (RECVER_DAT & RFLR_MODEMCONFIG1_CODINGRATE_MASK)| (value << 1);
	SX1276Write( REG_LR_MODEMCONFIG1, RECVER_DAT);
}

void  SX1276LoRaSetSignalBandwidth(unsigned char bw)
{
	unsigned char RECVER_DAT;
	RECVER_DAT = SX1276Read( REG_LR_MODEMCONFIG1);
	RECVER_DAT = (RECVER_DAT & RFLR_MODEMCONFIG1_BW_MASK) | (bw << 4);
	SX1276Write( REG_LR_MODEMCONFIG1, RECVER_DAT);
}

void  SX1276LoRaSetImplicitHeaderOn(BOOL enable)
{
	unsigned char RECVER_DAT;
	RECVER_DAT = SX1276Read( REG_LR_MODEMCONFIG1);
	RECVER_DAT = (RECVER_DAT & RFLR_MODEMCONFIG1_IMPLICITHEADER_MASK)| (enable);
	SX1276Write( REG_LR_MODEMCONFIG1, RECVER_DAT);
}

void  SX1276LoRaSetPayloadLength(unsigned char value)
{
	SX1276Write( REG_LR_PAYLOADLENGTH, value);
}

void  SX1276LoRaSetSymbTimeout(unsigned int value)
{
	unsigned char RECVER_DAT[2];
	RECVER_DAT[0] = SX1276Read( REG_LR_MODEMCONFIG2);
	RECVER_DAT[1] = SX1276Read( REG_LR_SYMBTIMEOUTLSB);
	RECVER_DAT[0] = (RECVER_DAT[0] & RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK)
					| ((value >> 8) & ~RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK);
	RECVER_DAT[1] = value & 0xFF;
	SX1276Write( REG_LR_MODEMCONFIG2, RECVER_DAT[0]);
	SX1276Write( REG_LR_SYMBTIMEOUTLSB, RECVER_DAT[1]);
}

void  SX1276LoRaSetMobileNode(BOOL enable)
{
	unsigned char RECVER_DAT;
	RECVER_DAT = SX1276Read( REG_LR_MODEMCONFIG3);
	RECVER_DAT = (RECVER_DAT & RFLR_MODEMCONFIG3_MOBILE_NODE_MASK)
					| (enable << 3);
	SX1276Write( REG_LR_MODEMCONFIG3, RECVER_DAT);
}

void  RF_RECEIVE(void)
{
	SX1276LoRaSetOpMode(Stdby_mode);
	SX1276Write(REG_LR_IRQFLAGSMASK, IRQN_RXD_Value);  //??????
	SX1276Write(REG_LR_HOPPERIOD, PACKET_MIAX_Value);
	SX1276Write( REG_LR_DIOMAPPING1, 0X00);
	SX1276Write( REG_LR_DIOMAPPING2, 0X00);
	SX1276Write( REG_LR_FIFORXBASEADDR, 0x80);
	SX1276Write( REG_LR_FIFOADDRPTR, 0x80);
	SX1276LoRaSetOpMode(Receiver_mode);
//	SX1276LoRaSetOpMode(receive_single);
}

void  SX1276LoRaSetPacketCrcOn(BOOL enable)
{
	unsigned char RECVER_DAT;
	RECVER_DAT = SX1276Read( REG_LR_MODEMCONFIG2);
	RECVER_DAT = (RECVER_DAT & RFLR_MODEMCONFIG2_RXPAYLOADCRC_MASK)
					| (enable << 2);
	SX1276Write( REG_LR_MODEMCONFIG2, RECVER_DAT);
}

void  SX1276LoRaFsk(Debugging_fsk_ook opMode)
{
	unsigned char opModePrev;
	opModePrev = SX1276Read(REG_LR_OPMODE);
	opModePrev &= 0x7F;
	opModePrev |= (unsigned char) opMode;
	SX1276Write( REG_LR_OPMODE, opModePrev);
}

void  SX1276LoRaSetRFPower(unsigned char power)
{
	SX1276Write( REG_LR_PADAC, 0x87);
	SX1276Write( REG_LR_PACONFIG, power_data[power]);
}
/*
function :you must call it ,the function is to init the module.
*/
void  SX1276LORA_INT(void)
{
	SX1276LoRaSetOpMode(Sleep_mode);  //??????0x01
	SX1276LoRaFsk(LORA_mode);	      // ??????,??????????
	SX1276LoRaSetOpMode(Stdby_mode);   // ???????
	SX1276Write( REG_LR_DIOMAPPING1, GPIO_VARE_1);
	SX1276Write( REG_LR_DIOMAPPING1, GPIO_VARE_1);
	SX1276Write( REG_LR_DIOMAPPING2, GPIO_VARE_2);
	SX1276LoRaSetRFFrequency();
	SX1276LoRaSetRFPower(powerValue);
	SX1276LoRaSetSpreadingFactor(SpreadingFactor);	 // ??????
	SX1276LoRaSetErrorCoding(CodingRate);		 //?????
	SX1276LoRaSetPacketCrcOn(TRUE );			  //CRC ????
	SX1276LoRaSetSignalBandwidth(Bw_Frequency);	 //??????
	SX1276LoRaSetImplicitHeaderOn(FALSE);		//????????
	SX1276LoRaSetPayloadLength(0xff);//0x22 timeout??
	SX1276LoRaSetSymbTimeout(0x3FF);
	SX1276LoRaSetMobileNode(TRUE ); 			 // ??????
//	RF_RECEIVE();
//    uartSendString("\r\ninit finish\r\n");
}
/*
function :if you want to send data,you can call it 
RF_TRAN_P:data
ASM_i:the length of the data
*/
void  FUN_RF_SENDPACKET(unsigned char *RF_TRAN_P,unsigned char LEN)
{
	unsigned char ASM_i;
	SX1276LoRaSetOpMode(Stdby_mode);
	SX1276Write( REG_LR_HOPPERIOD, 0);	//??????
	SX1276Write(REG_LR_IRQFLAGSMASK, IRQN_TXD_Value);	//??????
	SX1276Write( REG_LR_PAYLOADLENGTH, LEN);	 //?????
	SX1276Write( REG_LR_FIFOTXBASEADDR, 0);
	SX1276Write( REG_LR_FIFOADDRPTR, 0);
	for (ASM_i = 0; ASM_i < LEN; ASM_i++)
	{
		SX1276Write(0x00,*RF_TRAN_P);
		RF_TRAN_P++;
	}
	SX1276Write(REG_LR_DIOMAPPING1, 0x40);
	SX1276Write(REG_LR_DIOMAPPING2, 0x00);
	SX1276LoRaSetOpMode(Transmitter_mode);
}
