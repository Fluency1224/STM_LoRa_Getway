/********************************************************************************/
/***File Name:main.c                                                          ***/
/***Function :主函数入口                                                      ***/
/********************************************************************************/
#include "head.h"

extern int count;
extern uint16_t USART2_RX_BUF[USART_RX_LEN];  

/* Lora send and recv function */
u8 LoRa_Send(u8 *sen,u8 num)
{
	u8 test=0,delay=0;
	FUN_RF_SENDPACKET(sen,num);
	test = SX1276Read(0x12);
	while(test==0)
	{
		delay_us(20);
		test = SX1276Read(0x12);
		delay++;
		if(delay>0x6f)
			break;
	}
	SX1276Write(0x12,0xff);
	if(test==0x08)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

u8 LoRa_Receive(u8 *recv)
{
	u8 tem=0,delay=0;
	
	tem = SX1276Read(0x42);
	RF_RECEIVE();
	tem = SX1276Read(0x12);
	while(tem==0)
	{
		delay_us(20);
		tem = SX1276Read(0x12);
		delay++;
		if(delay>0x6f)
			return 0;
	}
	if(tem==0x40)
	{
		tem = SX1276Read(0x10);
		SX1276Write(0x0d,tem);
		tem = SX1276Read(0x13);
		SX1276ReadBuffer(0x00,recv,tem);
		SX1276Write(0x12,0xff);
		return 1;
	}
	if(tem==0x60)
	{
		*recv = SX1276Read(0x1d);
		*(recv+1) = SX1276Read(0x13);
		*(recv+2)= SX1276Read(0x0d);
		return 2;
	}
	return 3;
}

/* inin fuction*/
void system_init()
{
//	uint8_t my_data;
//	uint8_t my_stat;

	/*init program*/
	delay_init();	
	LED_Init();	
	sx1278GPIO_Ini();    
	PVD_Init();
	Dac1_Init();	
	USART2_Initialise(115200);
	
	/* reset LoRa*/
	GPIO_ResetBits(GPIOA, GPIO_Pin_11);
	GPIO_SetBits(GPIOA, GPIO_Pin_11);
	
	/* read LoRa */
//	my_data = SX1276Read(0x42);		// semtech芯片版本，默认0x12
//	my_stat = SX1276Read(0x44);		// 默认值0x2D

	/* sx1278 LoRa Mode init*/
	SX1276LORA_INT();
	
}

/*
 Main application entry point.
*/
int main( void )
{
	int i, ret;
	u8 RFBuffer[12];
	
	/* 系统初始化	*/
	system_init();
	
	while(1)
	{
		/* sx1278接收数组清零 */
		for(i = 0; i < 12; i++)
		{
			RFBuffer[i] = 0;
		}
		
		for(;;)
		{
			/* reset LoRa*/
			GPIO_ResetBits(GPIOA, GPIO_Pin_11);
			GPIO_SetBits(GPIOA, GPIO_Pin_11);
			/* sx1278 LoRa Mode init*/
			SX1276LORA_INT();
			/* sx1278接收数据 */
			ret = LoRa_Receive(RFBuffer);
			if(ret == 1)
				break;
		}
		/* 判头 != 0 返还 */
		if(RFBuffer[0] != 0)
		{
			LoRa_Send(RFBuffer, 12);
		}
		/* 节点数据UART上报 */
		for(i = 0; i < 12; i++)
		{
			delay_us(100);
			USART_SendData(USART2, RFBuffer[i]);
		}	
		/* UART 接收数组清零操作 */
		count = 0;
		for(i = 0; i < USART_RX_LEN; i++)
		{
			USART2_RX_BUF[i] = 0;
		}
	}
}



/*
* printf function
*/
int fputc(int ch, FILE *f)
{
	USART_SendData(USART2, (unsigned char) ch);
	while (!(USART2->SR & USART_FLAG_TXE));
	return (ch);
}

int GetKey(void) 
{
	while (!(USART2->SR & USART_FLAG_RXNE));
	return ((int)(USART2->DR & 0x1FF));
}
